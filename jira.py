import os
import re
import requests
import time
import json 

commit_message = os.getenv('CI_COMMIT_MESSAGE')
pattern = r"\b[A-Z]+-\d+\b"
issue_keys = re.findall(pattern, commit_message)
if len(issue_keys) == 0:
    print("Merge không có thông tin task jira")
    exit(1)
data1 = open('sast_result.json',encoding='utf8').read()
data1 = json.loads(data1)
data2 = open('secret_result.json',encoding='utf8').read()
data2 = json.loads(data2)
confirm = 0 if 'newFindings' not in data1 else len(data1['newFindings']) + 0 if 'newFindings' not in data2 else len(data2['newFindings'])
open = 0 if 'openFindings' not in data1 else len(data1['openFindings']) + 0 if 'openFindings' not in data2 else len(data2['openFindings'])
jira_url = os.getenv('JIRA_URL')
message = f"Code của task đã được merge, có {confirm} vấn đề mới và {open} vấn đề chưa xử lý. Chi tiết xem tại: http://103.82.39.245"
jdata = {"issues": issue_keys, "data" : {"message" : message}}
r = requests.post(jira_url, json=jdata)
