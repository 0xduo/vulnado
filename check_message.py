import os
import re
import requests
import time
import json
    
commit_message = os.getenv('CI_COMMIT_MESSAGE')
merge_title = os.getenv('CI_MERGE_REQUEST_TITLE')
project_url = os.getenv('CI_PROJECT_URL')
jira_url = os.getenv('JIRA_URL')
pattern = r"\b[A-Z]+-\d+\b"
print(merge_title, commit_message)
issue_keys = re.findall(pattern, commit_message)
if len(issue_keys) == 0:
    issue_keys = re.findall(pattern, merge_title)
    if len(issue_keys) == 0:
        print("Merge không có thông tin task jira")
        exit(1) 
